#define UNICODE
#define _UNICODE

#ifdef UNICODE
#define TCHAR WCHAR
#else
#define TCHAR CHAR
#endif

#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <tchar.h>
#include <stdio.h>
#include <time.h>

#define TYPE_FILE	1
#define TYPE_DIR	2
#define TYPE_DOT	3

#define ELEMENTS 12
#define MAXVALUE 150
#define SERVER_LENGHT MAX_PATH

#define IP_LENGHT 16
#define USER_LENGHT 25
#define DATE_LENGHT 21
#define TIME_LENGHT 9

typedef struct _parameter {
	LPCRITICAL_SECTION CSWriters;
	LPCRITICAL_SECTION CSReaders;
	LPHANDLE Writing;
	LPINT NumberOfReaders;
} PARAMETER;
typedef PARAMETER* LPPARAMETER;

typedef struct _record {
	TCHAR ip[IP_LENGHT];
	TCHAR user[USER_LENGHT];
	TCHAR date[DATE_LENGHT];
	TCHAR duration[TIME_LENGHT];
} RECORD;
typedef RECORD* LPRECORD;

INT ConvertIntoBinary(TCHAR* InFilename, TCHAR* OutFilename);
TCHAR** ReadServerFile(TCHAR* filename, LPINT n);
DWORD WINAPI ThreadFunction(LPVOID parameter);
VOID WaitThreadPool(LPHANDLE threads, INT nThreads);
VOID CloseHandles(LPHANDLE handles, INT nHandles);
VOID Reader(LPPARAMETER p, INT i);
VOID Writer(LPPARAMETER p, INT i);

TCHAR** Servers = NULL;
INT NumberOfServers = 0;

INT _tmain(int argc, LPTSTR argv[]) {
	if (argc < 3) {
		_ftprintf(stderr, _T("Parameter errors %s filename numberOfThreads\n"), argv[0]);
		Sleep(5000);
		return 1;
	}

	INT numberOfThreads = _ttoi(argv[2]);
	TCHAR ServerFile[MAX_PATH];

	setbuf(stdout, 0);

	ConvertIntoBinary(argv[1], ServerFile);
	Servers = ReadServerFile(ServerFile, &NumberOfServers);

	/* Data structure preparation */
	LPHANDLE Writing = (LPHANDLE)malloc(NumberOfServers * sizeof(HANDLE));
	LPINT NumberOfReaders = (LPINT)calloc(NumberOfServers, sizeof(INT));
	LPCRITICAL_SECTION CSReaders = (LPCRITICAL_SECTION)malloc(NumberOfServers * sizeof(CRITICAL_SECTION));
	LPCRITICAL_SECTION CSWriters = (LPCRITICAL_SECTION)malloc(NumberOfServers * sizeof(CRITICAL_SECTION));

	for (INT i = 0; i < NumberOfServers; i++) {
		InitializeCriticalSection(&CSReaders[i]);
		InitializeCriticalSection(&CSWriters[i]);
		Writing[i] = CreateMutex(NULL, FALSE, NULL);
	}

	PARAMETER p;
	p.CSReaders = CSReaders;
	p.CSWriters = CSWriters;
	p.NumberOfReaders = NumberOfReaders;
	p.Writing = Writing;

	/* Thread creation */
	LPHANDLE Threads = (LPHANDLE)malloc(numberOfThreads * sizeof(HANDLE));
	LPDWORD ThreadIds = (LPDWORD)malloc(numberOfThreads * sizeof(DWORD));
	for (INT i = 0; i < numberOfThreads; i++)
		Threads[i] = CreateThread(NULL, 0, ThreadFunction, &p, 0, &ThreadIds[i]);
	
	/* Wait threads and close handles */
	WaitThreadPool(Threads, numberOfThreads);
	CloseHandles(Threads, numberOfThreads);

	/* Frees */
	for (INT i = 0; i < NumberOfServers; i++)
		free(Servers[i]);
	free(Servers);
	free(Writing);
	free(NumberOfReaders);
	free(CSReaders);
	free(CSWriters);
	free(Threads);
	free(ThreadIds);

	Sleep(5000);
	return 0;
}

INT ConvertIntoBinary(TCHAR* InFilename, TCHAR* OutFilename) {
	_stprintf(OutFilename, _T("%s.bin"), InFilename);
	FILE* fpIn = _tfopen(InFilename, _T("r"));

	if (fpIn == NULL) {
		_ftprintf(stderr, _T("Cannot open input file %s. Error: %x\n"), 
			InFilename, GetLastError());
		return 1;
	}

	HANDLE hOut = CreateFile(OutFilename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hOut == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, _T("Cannot open output file %s. Error: %x\n"),
			OutFilename, GetLastError());
		fclose(fpIn);
		return 2;
	}

	INT n;
	if ((_ftscanf(fpIn, _T("%d"), &n)) == 1) {
		DWORD bytesWritten;
		WriteFile(hOut, &n, sizeof(INT), &bytesWritten, NULL);
		if (bytesWritten != sizeof(INT)) {
			_ftprintf(stderr, _T("Error writing file: %s\n"),
				OutFilename);
			fclose(fpIn);
			CloseHandle(hOut);
			return 3;
		}
	}

	TCHAR serverName[SERVER_LENGHT];
	while (_ftscanf(fpIn, _T("%s"), serverName) == 1) {
		// _tprintf(_T("Server read: %s\n"), serverName);
		DWORD bytesWritten;
		WriteFile(hOut, &serverName, sizeof(serverName), &bytesWritten, NULL);
		if (bytesWritten != sizeof(serverName)) {
			_ftprintf(stderr, _T("Error writing server: %s\n"),
				serverName);
			fclose(fpIn);
			CloseHandle(hOut);
			return 4;
		}
	}

	// _tprintf(_T("File %s correctly written\n"), OutFilename);
	fclose(fpIn);
	CloseHandle(hOut);
	return 0;
}

TCHAR** ReadServerFile(TCHAR* filename, LPINT n) {
	HANDLE hIn = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);
	if (hIn == INVALID_HANDLE_VALUE) {
		_ftprintf(stderr, _T("Cannot open output file %s. Error: %x\n"),
			filename, GetLastError());
		return NULL;
	}

	DWORD bytesRead;
	ReadFile(hIn, n, sizeof(INT), &bytesRead, NULL);
	// _tprintf(_T("Number of servers: %d\n"), *n);

	TCHAR** servers = (TCHAR**)malloc(*n * sizeof(TCHAR*));
	TCHAR serverName[SERVER_LENGHT];

	for (INT i = 0; i < *n; i++) {
		ReadFile(hIn, &serverName, sizeof(serverName), &bytesRead, NULL);
		servers[i] = _tcsdup(serverName);
	}

	CloseHandle(hIn);

	return servers;
}

DWORD WINAPI ThreadFunction(LPVOID parameter) {
	srand(GetCurrentThreadId());
	LPPARAMETER p = (LPPARAMETER)parameter;
	FLOAT n1 = (FLOAT)rand() / RAND_MAX,
		n2 = (FLOAT)rand() / RAND_MAX,
		n3 = (FLOAT)rand() / RAND_MAX;
	INT server = (INT)(n3 * NumberOfServers);

	/* Wait */
	Sleep(n1 * 1000);
	if (n2 < 0.5) {
		/* Reader */
		_tprintf(_T("[%5u] Reader ready\n"), GetCurrentThreadId());
		Reader(p, server);
		_tprintf(_T("[%5u] Reader returned\n"), GetCurrentThreadId());
	}
	else {
		/* Writer */
		_tprintf(_T("[%5u] Writer ready ...\n"), GetCurrentThreadId());
		Writer(p, server);
		_tprintf(_T("[%5u] Writer returned\n"), GetCurrentThreadId());
	}

	ExitThread(0);
}

DWORD FileType(LPWIN32_FIND_DATA pFileData) {
	BOOL IsDir;
	DWORD FType;
	FType = TYPE_FILE;
	IsDir = (pFileData->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
	if (IsDir)
		if (lstrcmp(pFileData->cFileName, _T(".")) == 0 || lstrcmp(pFileData->cFileName, _T("..")) == 0)
			FType = TYPE_DOT;
		else
			FType = TYPE_DIR;
	return FType;
}

VOID VisitFlatDirectory(LPTSTR SourcePathName) {
	HANDLE SearchHandle;
	WIN32_FIND_DATA FindData;
	DWORD FType, l;
	TCHAR NewPath[MAX_PATH], SearchPath[MAX_PATH];
	LONGLONG Duration = 0;

	_tprintf(_T("[%5u] Visiting directory: %s\n"), GetCurrentThreadId(), SourcePathName);
	_stprintf(SearchPath, _T("%s\\*"), SourcePathName);
	SearchHandle = FindFirstFile(SearchPath, &FindData);

	do {
		FType = FileType(&FindData);
		l = _tcslen(SourcePathName);
		if (SearchPath[l - 1] == '\\') {
			_stprintf(NewPath, _T("%s%s"), SourcePathName, FindData.cFileName);
		}
		else {
			_stprintf(NewPath, _T("%s\\%s"), SourcePathName, FindData.cFileName);
		}

		// NewPath contains the filename/pathname just found
		if (FType == TYPE_FILE) { // File
			_ftprintf(stdout, _T("[%5u] File found: %s\n"), GetCurrentThreadId(), NewPath);
			
			// Read file
			HANDLE hIn = CreateFile(NewPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL, NULL);
			if (hIn == INVALID_HANDLE_VALUE) {
				_ftprintf(stderr, _T("Cannot open output file %s. Error: %x\n"),
					NewPath, GetLastError());
				return;
			}

			DWORD bytesRead;
			RECORD r;
			INT hh, mm, ss;

			while (ReadFile(hIn, &r, sizeof(RECORD), &bytesRead, NULL) && bytesRead > 0) {
				_tprintf(_T("[%5u] Line read: %s %s %s %s\n"), GetCurrentThreadId(),
					r.ip, r.user, r.date, r.duration);
				_stscanf(r.duration, _T("%02d:%02d:%02d"), &hh, &mm, &ss); // Split duration
				Duration += (ss + mm * 60 + hh * 3600); // Add duration				
			}
			
			CloseHandle(hIn);
		}
	} while (FindNextFile(SearchHandle, &FindData));

	// Compute total duration
	INT hh, mm, ss;
	hh = Duration / 3600;
	Duration %= 3600;
	mm = Duration / 60;
	Duration %= 60;
	ss = Duration;

	// Print total duration
	_tprintf(_T("[%5u] Exiting directory: %s. Total duration: %02d:%02d:%02d\n"), GetCurrentThreadId(), SourcePathName,
		hh, mm, ss);

	FindClose(SearchHandle);
	return;
}

VOID Reader(LPPARAMETER p, INT i) {
	EnterCriticalSection(& p->CSReaders[i]);
	(p->NumberOfReaders[i])++;
	if (p->NumberOfReaders[i] == 1)
		WaitForSingleObject(p->Writing[i], 0);
	LeaveCriticalSection(& p->CSReaders[i]);

	/* Reading in concurrency */
	_tprintf(_T("[%5u] Reader started on server %d [%s] ...\n"), GetCurrentThreadId(), i, Servers[i]);
	VisitFlatDirectory(Servers[i]);
	_tprintf(_T("[%5u] ... Reader terminated on server %d [%s]\n"), GetCurrentThreadId(), i, Servers[i]);

	EnterCriticalSection(& p->CSReaders[i]);
	(p->NumberOfReaders[i])--;
	if (p->NumberOfReaders[i] == 0)
		ReleaseMutex(p->Writing[i]);
	LeaveCriticalSection(& p->CSReaders[i]);
}

VOID RandomModifications(LPTSTR SourcePathName) {
	HANDLE SearchHandle;
	WIN32_FIND_DATA FindData;
	DWORD FType, l;
	TCHAR NewPath[MAX_PATH], SearchPath[MAX_PATH];

	_tprintf(_T("[%5u] Visiting directory: %s\n"), GetCurrentThreadId(), SourcePathName);
	_stprintf(SearchPath, _T("%s\\*"), SourcePathName);
	SearchHandle = FindFirstFile(SearchPath, &FindData);

	do {
		FType = FileType(&FindData);
		l = _tcslen(SourcePathName);
		if (SearchPath[l - 1] == '\\') {
			_stprintf(NewPath, _T("%s%s"), SourcePathName, FindData.cFileName);
		}
		else {
			_stprintf(NewPath, _T("%s\\%s"), SourcePathName, FindData.cFileName);
		}

		// NewPath contains the filename/pathname just found
		if (FType == TYPE_FILE) { // File
			_ftprintf(stdout, _T("[%5u] File found: %s\n"), GetCurrentThreadId(), NewPath);
			// Edit file
			HANDLE hIn = CreateFile(NewPath, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS,
				FILE_ATTRIBUTE_NORMAL, NULL);
			if (hIn == INVALID_HANDLE_VALUE) {
				_ftprintf(stderr, _T("Cannot open output file %s. Error: %x\n"),
					NewPath, GetLastError());
				return;
			}

			DWORD Offset = rand() % (FindData.nFileSizeLow / sizeof(RECORD)); // Record to update
			LARGE_INTEGER pointer;
			pointer.QuadPart = (Offset) * sizeof(RECORD); // Position in byte

			DWORD bytesRead, bytesWritten;
			RECORD r;

			SetFilePointerEx(hIn, pointer, NULL, FILE_BEGIN);
			if(ReadFile(hIn, &r, sizeof(RECORD), &bytesRead, NULL) && bytesRead > 0) {
				_tprintf(_T("[%5u] Line read:    %s %s %s %s\n"), GetCurrentThreadId(),
					r.ip, r.user, r.date, r.duration);
			}
			_stprintf(r.date, _T("2018/%02d/%02d:%02d:%02d:%02d"),
				rand() % 12 + 1, rand() % 28 + 1, rand() % 24 + 1, rand() % 60, rand() % 60);
			_stprintf(r.duration, _T("%02d:%02d:%02d"),
				rand() % 3, rand() % 60, rand() % 60);

			SetFilePointerEx(hIn, pointer, NULL, FILE_BEGIN);
			if(WriteFile(hIn, &r, sizeof(RECORD), &bytesWritten, NULL) && bytesWritten == sizeof(RECORD)) {
				_tprintf(_T("[%5u] Line written: %s %s %s %s\n"), GetCurrentThreadId(),
					r.ip, r.user, r.date, r.duration);
			}

			CloseHandle(hIn);
		}
	} while (FindNextFile(SearchHandle, &FindData));

	_tprintf(_T("[%5u] Exiting directory: %s.\n"), GetCurrentThreadId(), SourcePathName);

	FindClose(SearchHandle);
	return;

}

VOID Writer(LPPARAMETER p, INT i) {
	EnterCriticalSection(&p->CSWriters[i]);
	WaitForSingleObject(p->Writing[i], 0);

	/* Writing in mutual exclusion */
	_tprintf(_T("[%5u] Writer started on server %d [%s] ...\n"), GetCurrentThreadId(), i, Servers[i]);
	RandomModifications(Servers[i]);
	_tprintf(_T("[%5u] ... Writer terminated on server %d [%s]\n"), GetCurrentThreadId(), i, Servers[i]);

	ReleaseMutex(p->Writing[i]);
	LeaveCriticalSection(&p->CSWriters[i]);
}

VOID WaitThreadPool(LPHANDLE threads, INT nThreads) {
	for (INT i = 0; i < nThreads; i += MAXIMUM_WAIT_OBJECTS)
		WaitForMultipleObjects(min(nThreads - i, MAXIMUM_WAIT_OBJECTS), &threads[i], TRUE, INFINITE);
}

VOID CloseHandles(LPHANDLE handles, INT nHandles) {
	for (INT i = 0; i < nHandles; i += MAXIMUM_WAIT_OBJECTS)
		CloseHandle(handles[i]);
}