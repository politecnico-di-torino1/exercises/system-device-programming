# Exercise 2

The main thread allocates the data structures, declares a **signal handler**, creates two threads and then simply waits for their termination. Both threads, before performing a `post` or a `wait`, sleep a random time.

The key point is the `wait_with_timeout` function: it sets a timer which, on expiration, raises a `SIGALRM` caught by the signal handler. The signal handler, simply sets a global variable in order to know how if the system call `wait` was interrupted or not and performs a post on the semaphore.
