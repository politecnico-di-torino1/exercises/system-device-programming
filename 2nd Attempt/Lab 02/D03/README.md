# Exercise 3

The main procedure is similar to the previous one, but for the declaration of a signal handler.

In fact, function `wait_with_timeout` does not raise a `SIGALRM`, instead is exploits the system call `sem_timedwait`, which automatically embeds the previous behaviour.
