#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <errno.h>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define	ARGUMENT_ERROR	1
#define MALLOC_ERROR	2

sem_t* s;

sem_t* initSemaphore(unsigned int value);
void freeSemaphore(sem_t* s);

void* th1(void* param);
void* th2(void* param);
int wait_with_timeout(sem_t* s, int tmax);

int main(int argc, char** argv) {
	pthread_t tid1, tid2;
	int *tmax;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s tmax\n", argv[0]);
		exit(ARGUMENT_ERROR);
	}

	srand(time(NULL));
	setbuf(stdout, 0);

	tmax = malloc(sizeof(int));
	*tmax = atoi(argv[1]);
	s = initSemaphore(0);

	pthread_create(&tid1, NULL, th1, (void*) tmax);
	pthread_create(&tid2, NULL, th2, NULL);

	pthread_join(tid1, NULL);
	pthread_join(tid2, NULL);
	freeSemaphore(s);

	exit(0);
}

void* th1(void* param) {
	int* tmax = (int*) param;
	int msec = rand() % 5 + 1;
	struct timespec ts = {.tv_sec = 0, .tv_nsec = msec * 1000 * 1000};

	nanosleep(&ts, NULL);
	printf("[Thread 1] Waiting on semaphore after %d milliseconds\n", msec);

	if (wait_with_timeout(s, *tmax) == 0) {
		printf("[Thread 1] Wait returned normally\n");
	} else {
		printf("[Thread 1] Timeout occurred\n");
	}

	pthread_exit(0);
}

void* th2(void* param) {
	int msec, nsec, sec;
	struct timespec ts;

	msec = rand() % 9001 + 1000;	// Extract random number (milliseconds)
	sec = msec / 1000;				// Extract seconds
	nsec = (msec % 1000) * 1000;	// Extract remainder, convert in nanoseconds
	ts.tv_sec = sec;
	ts.tv_nsec = nsec;

	nanosleep(&ts, NULL);
	printf("[Thread 2] Signalling on semaphore after %d milliseconds\n", msec);
	sem_post(s);

	pthread_exit(0);
}

int wait_with_timeout(sem_t* s, int tmax) {
	int sec, nsec;
	struct timespec ts;

	sec = tmax / 1000;
	nsec = (tmax % 1000) * 1000;

	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += sec;
	ts.tv_nsec += nsec * 1000000;
	if (ts.tv_nsec >= 1000000000) {
		ts.tv_sec++;
		ts.tv_nsec -= 1000000000;
	}

	return sem_timedwait(s, &ts);
}

sem_t* initSemaphore(unsigned int value) {
	sem_t* s = malloc(sizeof(sem_t));

	if (s == NULL) {
		fprintf(stderr, "Impossible to allocate sempaphore\n");
		exit(MALLOC_ERROR);
	}

	sem_init(s, 0, value);
	return s;
}

void freeSemaphore(sem_t* s) {
	sem_destroy(s);
	free(s);
}
