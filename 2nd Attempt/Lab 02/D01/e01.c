#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <semaphore.h>
#include <signal.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MALLOC_ERROR	2
#define	OPEN_FILE_ERROR	3

#define N_THREADS	2
#define STR_LENGTH	50

int GLOBAL, TERMINATED;
sem_t *me, *readData, *writeData;

typedef struct {
	char* filename;
	int id;
} param_t;

void handler(int signo);
void* client(void* param);
int openFileUnix(char* filename);
param_t* prepareParam(char* filename, int id);
sem_t* initSemaphore(unsigned int value);
void freeSemaphore(sem_t* s);

int main(int argc, char** argv) {
	char filename1[STR_LENGTH],
		filename2[STR_LENGTH];
	pthread_t tid1, tid2;
	long int requests = 0;
	param_t *p1, *p2;

	sprintf(filename1, "fv1.b");
	sprintf(filename2, "fv2.b");

	me = initSemaphore(1);
	readData = initSemaphore(0);
	writeData = initSemaphore(0);

	setbuf(stdout, 0);
	srand(time(NULL));
	signal(SIGUSR1, handler);

	p1 = prepareParam(filename1, 1);
	p2 = prepareParam(filename2, 2);

	pthread_create(&tid1, NULL, client, (void*) p1);
	pthread_create(&tid2, NULL, client, (void*) p2);

	while (1) {
		/* Server */

		sem_wait(readData);
		if(TERMINATED >= N_THREADS)
			break;
		usleep(rand() % 1000);
		GLOBAL *= 3; // Computation
		sem_post(writeData);
		requests++;
	}

	pthread_join(tid1, NULL);
	pthread_join(tid2, NULL);

	freeSemaphore(me);
	freeSemaphore(readData);
	freeSemaphore(writeData);

	printf("[Server] Terminated after %ld requests\n", requests);

	exit(0);
}

void* client(void* param) {
	param_t* arg = (param_t*) param;
	int fd = openFileUnix(arg->filename);
	int value;
	long int requests = 0;

	printf("[Client %d] Working on file %s on fd %d\n", arg->id, arg->filename, fd);

	while (read(fd, &value, sizeof(int)) == sizeof(int)) {
		/* Save in global memory in mutual exclusion */

		requests++;
		sem_wait(me);
		usleep(rand() % 1000);
		GLOBAL = value;
		sem_post(readData);
		printf("[Client %d] (%02ld) Read number = %d\n", arg->id, requests, GLOBAL);
		sem_wait(writeData); // Wait for server answer
		printf("[Client %d] (%02ld) Result = %d\n", arg->id, requests, GLOBAL);
		sem_post(me);
	}

	printf("[Client %d] Terminated after %ld readings\n", arg->id, requests);
	raise(SIGUSR1);

	pthread_exit((void*) requests);
}

void handler(int signo) {
	if (signo != SIGUSR1)
		return;
	if (++TERMINATED == N_THREADS)
		sem_post(readData);
}

int openFileUnix(char* filename) {
	int fd = open(filename, O_RDONLY);

	if (fd < 0) {
		fprintf(stderr, "Error opening file %s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fd;
}

param_t* prepareParam(char* filename, int id) {
	param_t* p = malloc(sizeof(param_t));

	if(p == NULL) {
		fprintf(stderr, "Impossible to allocate parameter\n");
		exit(MALLOC_ERROR);
	}

	p->filename = filename;
	p->id = id;

	return p;
}

sem_t* initSemaphore(unsigned int value) {
	sem_t* s = malloc(sizeof(sem_t));

	if (s == NULL) {
		fprintf(stderr, "Impossible to allocate sempaphore\n");
		exit(MALLOC_ERROR);
	}

	sem_init(s, 0, value);
	return s;
}

void freeSemaphore(sem_t* s) {
	sem_destroy(s);
	free(s);
}
