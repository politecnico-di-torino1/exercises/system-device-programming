# Exercise 2

Follow the steps for creating the kernel and the bootable hard `hd.img` file in directory `MK/paging`.

Use the tutorial in http://www.jamesmolloy.co.uk/tutorial_html/ to understand the main and the functions of this minimal kernel.

- Change the file main.c to verify which is the first address that produces a page fault;
- Use the functions `monitor_write`, `monitor_write_dec`, `monitor_write_hex` to print lines such as:
```
Normal access at address 0x0 at page 0
Normal access at address 0x1000 at page 1
```

Take note of the number of pages that your kernel has allocated.

# Exercise 4

Implement a program in C that takes a single argument k from the command line.

- The program creates two vectors (`v1` and `v2`) of dimension `k`, and a matrix (`mat`) of dimension `k`x`k`, which are filled with random numbers in the range [-0.5, 0.5];
- The main thread `k` threads and waits the termination of the threads;
- Each thread `i` performs the product of the `i`-th row vector of `mat` and `v2`, which produces
the `i`-th element of vector `v`;
- One of the created threads, the last one, terminating its product operation, performs the final operation `result = v1 * v`, and prints the result.
