# Exercise 2

In order to find the first address which causes a page fault, a loop has been used.

The address must be incremented with the page size (4 KB, i.e., `0x00001000`) at each iteration.

As shown in figure, the first page causing a page fault is the 256-th, which tries to access the address `0x00109000`.

![Page fault](./img/first_page_fault.jpg)
