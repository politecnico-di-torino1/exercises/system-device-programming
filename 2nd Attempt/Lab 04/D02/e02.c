#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"

int main(struct multiboot *mboot_ptr)
{
	// Initialise all the ISRs and segmentation
	init_descriptor_tables();
	// Initialise the screen (by clearing it)
	monitor_clear();

	initialise_paging();
	monitor_write("Hello, paging world!\n");

	u32int *ptr;
	u32int reference, do_page_fault;
	u32int i = 0, address;
	for (; i < 10000; i++) {
		address = i * (0x1000); // 4KB
		monitor_write("accessing page ");
		monitor_write_dec(i);
		monitor_write(" address ");
		monitor_write_hex(address);
		monitor_write(".\n");
		ptr = (u32int*) address;
		do_page_fault = *ptr;
	}

	return 0;
}
