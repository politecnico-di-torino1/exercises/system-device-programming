#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

#define	ARGUMENT_ERROR	1
#define MALLOC_ERROR	2
#define	OPEN_FILE_ERROR	3
#define	WRITE_ERROR		3

#define EVEN	0
#define ODD		1

#define STR_LENGTH	50

int* generateArray(int n, int min, int max, int type);
void printArray(FILE* fp, int* v, int n);
void sortArray(int* v, int n);

FILE* openFileText(char* filename);
FILE* openFileAnsi(char* filename);
int openFileUnix(char* filename);
void writeFileAnsi(FILE* fp, int* v, int n);
void writeFileUnix(int fd, int* v, int n);

void child(int n, char* suffix, int min, int max, int type);

int main(int argc, char** argv) {
	pid_t pid;
	int status;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s n1 n2\n", argv[0]);
		exit(ARGUMENT_ERROR);
	}

	srand(time(NULL));

	if(fork() == 0) {
		child(atoi(argv[1]), "v1", 10, 100, EVEN);
		return 1;
	}

	if(fork() == 0) {
		child(atoi(argv[2]), "v2", 21, 101, ODD);
		return 2;
	}

	pid = wait(&status);
	printf("Process %d terminated with return code %d\n", pid, WEXITSTATUS(status));
	pid = wait(&status);
	printf("Process %d terminated with return code %d\n", pid, WEXITSTATUS(status));

	exit(0);
}

void child(int n, char* suffix, int min, int max, int type) {
	int *v = NULL;
	char filenameA[STR_LENGTH],
		filenameU[STR_LENGTH],
		filenameT[STR_LENGTH];
	FILE *fpA = NULL, *fpT = NULL;
	int fd;

	sprintf(filenameA, "fv1_ansi_%s.b", suffix);
	sprintf(filenameU, "fv1_unix_%s.b", suffix);
	sprintf(filenameT, "fv1_%s.txt", suffix);

	v = generateArray(n, min, max, type);

	printf("[%d]\tOriginal array: ", getpid());
	printArray(stdout, v, n);

	sortArray(v, n);

	fpT = openFileText(filenameT);
	printArray(fpT, v, n);

	fpA = openFileAnsi(filenameA);
	fd = openFileUnix(filenameU);

	writeFileAnsi(fpA, v, n);
	writeFileUnix(fd, v, n);

	/* Cleanup */
	free(v);
	fclose(fpT);
	fclose(fpA);
	close(fd);
}

FILE* openFileText(char* filename) {
	FILE* fp = fopen(filename, "wb");

	if (fp == NULL) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fp;
}

FILE* openFileAnsi(char* filename) {
	FILE* fp = fopen(filename, "wb");

	if (fp == NULL) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fp;
}

int openFileUnix(char* filename) {
	int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0664);

	if (fd < 0) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fd;
}

int* generateArray(int n, int min, int max, int type) {
	int i, val;
	int* v = malloc(sizeof(int) * n);

	if (v == NULL) {
		fprintf(stderr, "malloc error\n");
		exit(MALLOC_ERROR);
	}

	for (i = 0; i < n; i++) {
		val = rand() % (max - min) + min;
		if (type == EVEN && val % 2 == 1)
			val = val + 1;
		if (type == ODD && val % 2 == 0)
			val = val + 1;
		v[i] = val;
	}

	return v;
}

void sortArray(int *v, int n) {
	int i, j, tmp;

	for (i = 0; i < n - 1; i++)
		for (j = i + 1; j < n; j++)
			if (v[i] > v[j]) {
				tmp = v[i];
				v[i] = v[j];
				v[j] = tmp;
			}
}

void writeFileAnsi(FILE* fp, int* v, int n) {
	size_t expected = n;
	size_t written = fwrite(v, sizeof(*v), n, fp);
	if (written != expected) {
		fprintf(stderr, "[Ansi] Write error. Expected %lu, Written %lu\n", expected, written);
		exit(WRITE_ERROR);
	}
}

void writeFileUnix(int fd, int* v, int n) {
	size_t expected = sizeof(*v) * n;
	size_t written = write(fd, v, expected);
	if (written != expected) {
		fprintf(stderr, "[Unix] write error. Expected %lu, Written %lu\n", expected, written);
		exit(WRITE_ERROR);
	}
}

void printArray(FILE* fp, int* v, int n) {
	int i;

	for (i = 0; i < n; i++)
		fprintf(fp, "%d; ", v[i]);
	fprintf(fp, "\n");
}
