# Exercise 3

The solution of *exercise 3* is more sophisticated and uses `pthread` library, exploting system calls `pthread_create` and `pthread_join` to create and wait threads respectively.

The file is read character by character in a `while` loop, but given that some concurrency constraints must be enforced:

- First character is read before the cycle;
- Last character is used by **output** thread after the cycle.
