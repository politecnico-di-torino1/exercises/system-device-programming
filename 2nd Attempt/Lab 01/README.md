# Exercise 1

Implement a C program that

- Takes from the command line two integer numbers `n1`, `n2`;
- Allocates two vectors v1 and v2, of dimensions `n1` and `n2`, respectively;
- Fills `v1` with `n1` random even integer numbers between 10-100;
- Fills `v2` with `n2` random odd integer numbers between 21-101;
- Sort `v1` and `v2` (increasing values);
- Save the content of vectors `v1` and `v2` in two text files `fv1.txt` and `fv2.txt`, respectively;
- Save the content of vectors `v1` and `v2` in two binary files `fv1.b` and `fv2.b`, respectively.

Use command `od` for verifying the content of files `v1`, `v2` (`man od` for help)

# Exercise 2

Implement a concurrent C program that performs the same task of *exercise 1.1*, but in concurrency.

The main process, creates two children, and waits their termination, collecting their exit status. Each child creates a file with filename (`v1` or `v2`) corresponding to its identity, and returns its identity (`1` or `2`).

# Exercise 3

Write a concurrent program in C language, using Pthreads, which takes a filename as an argument in the command line, and implements the precedence graph drawn below. It represents the sequence of operations that a main thread performs for processing the content of a text file.

Each node of the graph represents a thread. All threads are created by the main thread.

There are three types of threads:

- The Input thread (the main thread), which gets the next character from the file in a global
variable `next`;
- Each Processing thread transforms to upper case the content of a global variable `this`;
- Each Output thread prints the content of a global variable `last`.

The small circles in the graph represent synchronization points, where the main thread appropriately updates the content of the global variables.

The first small circle, for example, corresponds to the main thread executing the statement `this = next` so that it can create, after this statement, the thread P<sub>1</sub> and get in `next` the next character in concurrency.

The other small circles correspond to similar sequential statements.

![Start](./img/start.jpg)
![End](./img/end.jpg) 
