#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define	ARGUMENT_ERROR	1
#define MALLOC_ERROR	2
#define	OPEN_FILE_ERROR	3
#define	WRITE_ERROR		3

#define EVEN	0
#define ODD		1

#define STR_LENGTH	50

int* generateArray(int n, int min, int max, int type);
void printArray(FILE* fp, int* v, int n);
void sortArray(int* v, int n);

FILE* openFileText(char* filename);
FILE* openFileAnsi(char* filename);
int openFileUnix(char* filename);
void writeFileAnsi(FILE* fp, int* v, int n);
void writeFileUnix(int fd, int* v, int n);

int main(int argc, char** argv) {
	int n1, n2;
	int *v1, *v2;
	FILE *fp1, *fp2, *fpT1, *fpT2;
	int fd1, fd2;

	char filenameA1[STR_LENGTH],
		filenameA2[STR_LENGTH],
		filenameU1[STR_LENGTH],
		filenameU2[STR_LENGTH],
		filenameT1[STR_LENGTH],
		filenameT2[STR_LENGTH];

	if (argc < 3) {
		fprintf(stderr, "Usage: %s n1 n2\n", argv[0]);
		exit(ARGUMENT_ERROR);
	}

	n1 = atoi(argv[1]);
	n2 = atoi(argv[2]);

	sprintf(filenameA1, "fv1_ansi.b");
	sprintf(filenameA2, "fv2_ansi.b");
	sprintf(filenameU1, "fv1_unix.b");
	sprintf(filenameU2, "fv2_unix.b");
	sprintf(filenameT1, "fv1.txt");
	sprintf(filenameT2, "fv2.txt");

	srand(time(NULL));
	v1 = generateArray(n1, 10, 100, EVEN);
	v2 = generateArray(n2, 21, 101, ODD);

	printf("Original arrays:\n");
	printArray(stdout, v1, n1);
	printArray(stdout, v2, n2);

	sortArray(v1, n1);
	sortArray(v2, n2);

	fpT1 = openFileText(filenameT1);
	fpT2 = openFileText(filenameT2);

	printArray(fpT1, v1, n1);
	printArray(fpT2, v2, n2);

	fp1 = openFileAnsi(filenameA1);
	fp2 = openFileAnsi(filenameA2);
	fd1 = openFileUnix(filenameU1);
	fd2 = openFileUnix(filenameU2);

	writeFileAnsi(fp1, v1, n1);
	writeFileAnsi(fp2, v2, n2);
	writeFileUnix(fd1, v1, n1);
	writeFileUnix(fd2, v2, n2);

	/* Free memory */
	free(v1);
	free(v2);

	/* Close files */
	close(fd1);
	close(fd2);
	fclose(fp1);
	fclose(fp2);
	fclose(fpT1);
	fclose(fpT2);

	exit(0);
}

FILE* openFileText(char* filename) {
	FILE* fp = fopen(filename, "wb");

	if (fp == NULL) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fp;
}

FILE* openFileAnsi(char* filename) {
	FILE* fp = fopen(filename, "wb");

	if (fp == NULL) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fp;
}

int openFileUnix(char* filename) {
	int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0664);

	if (fd < 0) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	return fd;
}

int* generateArray(int n, int min, int max, int type) {
	int i, val;
	int* v = malloc(sizeof(int) * n);

	if (v == NULL) {
		fprintf(stderr, "malloc error\n");
		exit(MALLOC_ERROR);
	}

	for (i = 0; i < n; i++) {
		val = rand() % (max - min) + min;
		if (type == EVEN && val % 2 == 1)
			val = val + 1;
		if (type == ODD && val % 2 == 0)
			val = val + 1;
		v[i] = val;
	}

	return v;
}

void sortArray(int *v, int n) {
	int i, j, tmp;

	for (i = 0; i < n - 1; i++)
		for (j = i + 1; j < n; j++)
			if (v[i] > v[j]) {
				tmp = v[i];
				v[i] = v[j];
				v[j] = tmp;
			}
}

void writeFileAnsi(FILE* fp, int* v, int n) {
	size_t expected = n;
	size_t written = fwrite(v, sizeof(*v), n, fp);
	if (written != expected) {
		fprintf(stderr, "[Ansi] Write error. Expected %lu, Written %lu\n", expected, written);
		exit(WRITE_ERROR);
	}
}

void writeFileUnix(int fd, int* v, int n) {
	size_t expected = sizeof(*v) * n;
	size_t written = write(fd, v, expected);
	if (written != expected) {
		fprintf(stderr, "[Unix] write error. Expected %lu, Written %lu\n", expected, written);
		exit(WRITE_ERROR);
	}
}

void printArray(FILE* fp, int* v, int n) {
	int i;

	for (i = 0; i < n; i++)
		fprintf(fp, "%d; ", v[i]);
	fprintf(fp, "\n");
}
