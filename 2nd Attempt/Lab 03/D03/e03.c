#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <fcntl.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define ARGUMENT_ERROR	2
#define	OPEN_FILE_ERROR	3
#define	STAT_ERROR		4
#define	WRITE_ERROR		5

#define ARRAY_SIZE		50

typedef struct param_t {
	int left, right;
} param_t;

param_t* prepareParam(int l, int r) {
	param_t* p = malloc(sizeof(*p));
	p->left = l;
	p->right = r;
	return p;
}

int* generateArray(int n, int min, int max) {
	int i, *v = malloc(sizeof(int) * n);
	for (i = 0; i < n; i++)
		v[i] = rand() % (max - min) + min;
	return v;
}

void printArray(int *v, int n) {
	int i;
	for (i = 0; i < n; i++)
		printf("%d; ", v[i]);
	printf("\n");
}

void writeFile(char *filename, int* v, int n) {
	int fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0664);
	size_t expected = sizeof(*v) * n;
	size_t written = write(fd, v, expected);
	if (written != expected) {
		exit(WRITE_ERROR);
	}
	close(fd);
}

void createFile(char *filename, int n) {
	int *v = generateArray(n, -100, 100);
	writeFile(filename, v, n);
	free(v);
}

int size, *v;

int openFile(char* filename, int* size);
void quicksort(int* v, int n);
void *thQuicksort(void* param);

int main(int argc, char** argv) {
	int fd, length;
	int n;

	if(argc < 3) {
		fprintf(stderr, "Usage: %s filename size\n", argv[0]);
		exit(ARGUMENT_ERROR);
	}

	srand(time(NULL));
	setbuf(stdout, 0);
	createFile(argv[1], ARRAY_SIZE);
	size = atoi(argv[2]);

	// Open file
	fd = openFile(argv[1], &length);
	n = length / sizeof(int);
	v = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	printArray(v, n);
	quicksort(v, n);
}

int openFile(char* filename, int* length) {
	int fd = open(filename, O_RDWR);
	struct stat buf;

	if (fd < 0) {
		fprintf(stderr, "Error opening file%s\n", filename);
		exit(OPEN_FILE_ERROR);
	}

	if (fstat(fd, &buf) < 0) {
		fprintf(stderr, "Error fstat for file%s\n", filename);
		exit(STAT_ERROR);
	}

	*length = buf.st_size;
	return fd;
}

void quicksort(int*v, int n) {
	param_t* p = prepareParam(0, n - 1);
	pthread_t tid;

	pthread_create(&tid, NULL, thQuicksort, (void*) p);
	pthread_join(tid, NULL);
	free(p);
}

void *thQuicksort(void* param) {
	param_t* p = (param_t*) param;
	int i, j, x, tmp;
	pthread_t tid1, tid2;
	param_t *p1, *p2;

	if (p->left >= p->right) {
		return 0;
	}

	x = v[p->left];
	i = p->left - 1;
	j = p->right + 1;
	while (i < j) {
		while (v[--j] > x);
		while (v[++i] < x);
		if (i < j) {
			tmp = v[i];
			v[i] = v[j];
			v[j] = tmp;
		}
	}

	p1 = prepareParam(p->left, j);
	p2 = prepareParam(j + 1, p->right);

	if(p->right - p->left < size) {
		pthread_create(&tid1, NULL, thQuicksort, (void*) p1);
		pthread_create(&tid2, NULL, thQuicksort, (void*) p2);
		pthread_join(tid1, NULL);
		pthread_join(tid2, NULL);
	} else {
		thQuicksort((void*) p1);
		thQuicksort((void*) p2);
	}

	free(p1);
	free(p2);

	return 0;
}
