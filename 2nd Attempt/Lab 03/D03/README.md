# Exercise 3

The solution of *exercise 3* is a straightforward implementation of **quicksort** algorithm which replaces recursive calls with thread creation.

The input file is randomly generated and then read/written using `mmap`, its name is passed on the command
line.

Given that all threads working on the array must know left and right indexes of the array, a structure is used to gather these attributes.

If the difference between the array indexes is less than a value `size`, given as an argument of the command line, sorting is performed by the standard quicksort algorithm. In this case, the same thread function in order to not duplicate the code.
