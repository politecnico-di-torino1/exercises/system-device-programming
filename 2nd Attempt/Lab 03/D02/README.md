# Exercise 2

The solution of *exercise 2* consists of the implementation of `init`, `wait` and `post` system calls using only condition variables and mutex, without using the `semaphore.h` library. A structure `sema_t` has been defined, which contains a condition and a counter protected by a mutex.

- `sema_init` simply allocates the needed items and sets the initial value of the semaphore;
- `sema_wait` blocks the calling thread if the counter is lower than zero and decrements it;
- `sema_post` increments the counter and awakes a waiting thread, if any.

In order to complete the solution, in a similar way `sema_destroy` is a "custom" implementation of `sem_destroy`.
