# Exercise 1

The solution of *exercise 1* is an extension of the well-known **producer & consumer** pattern.

- The producer generates a random number and evaluate where to place the produced item;
- The consumer performs a `sem_trywait`, trying to dequeue an item from the urgent buffer, otherwise it performs a "normal" `sem_wait` on the normal buffer.
