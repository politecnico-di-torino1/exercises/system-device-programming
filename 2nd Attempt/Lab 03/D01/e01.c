#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define NORMAL_SIZE		10
#define URGENT_SIZE		3
#define LIMIT			500

/* Semaphore */
sem_t* initSemaphore(unsigned int value) {
	sem_t* s = malloc(sizeof(sem_t));
	sem_init(s, 0, value);
	return s;
}

void freeSemaphore(sem_t* s) {
	printf("Freeing semaphore...\n");
	sem_destroy(s);
	free(s);
}

/* Buffer */
typedef struct buffer_t {
	long long *buffer;
	int size, in, out;
	sem_t *free, *full;
	char *name;
} buffer_t;

buffer_t* prepareBuffer(const int size, const char* name) {
	buffer_t* b = malloc(sizeof(*b));

	b->buffer = malloc(sizeof(b->buffer) * size);
	b->size = size;
	b->in = b->out = 0;
	b->name = strdup(name);
	b->free = initSemaphore(size);
	b->full = initSemaphore(0);

	return b;
}

void freeBuffer(buffer_t* b) {
	printf("Freeing heap space...\n");
	freeSemaphore(b->free);
	freeSemaphore(b->full);
	free(b->buffer);
	free(b->name);
	free(b);
}

/* Prototypes */
long long currentTimestamp();
void* producer(void* param);
void* consumer(void* param);
void enqueue(buffer_t* b, long long e);
long long dequeue(buffer_t* b);

/* Global variables */
buffer_t *normal, *urgent;

int main(int argc, char** argv) {
	pthread_t tidP, tidC;

	setbuf(stdout, 0);
	srand(time(NULL));

	normal = prepareBuffer(NORMAL_SIZE, "normal");
	urgent = prepareBuffer(URGENT_SIZE, "urgent");

	pthread_create(&tidP, NULL, producer, NULL);
	pthread_create(&tidC, NULL, consumer, NULL);

	pthread_join(tidP, NULL);
	pthread_join(tidC, NULL);

	freeBuffer(normal);
	freeBuffer(urgent);

	exit(0);
}

void* producer(void* param) {
	int i, waitTime;
	long long ms;

	for (i = 0; i < LIMIT; i++) {
		waitTime = rand() % 10;
		usleep(waitTime * 1000);
		ms = currentTimestamp();

		if (rand() % 100 <= 80) {
			enqueue(normal, ms);
		} else {
			enqueue(urgent, ms);
		}
	}
	pthread_exit(0);
}

void* consumer(void* param) {
	int i;

	for (i = 0; i < LIMIT; i++) {
		usleep(10 * 1000);

		if (sem_trywait(urgent->full) == 0) {
			dequeue(urgent);
		} else {
			sem_wait(normal->full);
			dequeue(normal);
		}
	}

	pthread_exit(0);
}

void enqueue(buffer_t* b, long long e) {
	printf("putting %lld in buffer %s in position %d\n", e, b->name, b->in);
	sem_wait(b->free);
	b->buffer[b->in] = e;
	b->in = (b->in + 1) % b->size;
	sem_post(b->full);
}

long long dequeue(buffer_t* b) {
	long long e = b->buffer[b->out];

	printf("getting an element from buffer %s in position %d", b->name, b->out);
	b->out = (b->out + 1) % b->size;
	sem_post(b->free);
	printf(": %lld\n", e);

	return e;
}

long long currentTimestamp() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	long long ms = tv.tv_sec * 1000LL + tv.tv_usec / 1000;
	return ms;
}
